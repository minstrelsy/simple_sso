<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }
    
    public function testAction() {
    	$otpMapper = new Application_Model_OtpMapper();
    	$otp = $otpMapper->build(array(), Application_Model_OtpMapper::TYPE_LOGIN);
    	
    	$this->view->otp = $otp;
    }
    
    public function createAction() {
    	
    	$query = 'CREATE TABLE IF NOT EXISTS `simplesso_otp` (
  `otp_id` int(11) NOT NULL AUTO_INCREMENT,
  `data` text,
  `context` tinyint(2) unsigned NOT NULL,
  `otp_value` char(32) NOT NULL,
  `otp_data_insert` datetime NOT NULL,
  `otp_data_consumed` datetime DEFAULT NULL,
  PRIMARY KEY (`otp_id`),
  UNIQUE KEY `otp_value` (`otp_value`),
  KEY `context` (`context`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;';
    	
    	$otpMapper = new Application_Model_OtpMapper();
    	$db = $otpMapper->getDbTable()->getAdapter();
    	
    	$db->query($query);
    	
    }
    
    public function dropAction() {
    	
    	$query = 'DROP TABLE `simplesso_otp`';
    	
    	$otpMapper = new Application_Model_OtpMapper();
    	$db = $otpMapper->getDbTable()->getAdapter();
    	
    	$db->query($query);
    }


}

