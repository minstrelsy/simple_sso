<?php
/**
 * Otp DbTable
 * 
 * @author sergio.rinaudo
 */
class Application_Model_DbTable_Otp extends Zend_Db_Table_Abstract
{
	/**
	 * DbTable name
	 */
	protected $_name = 'simplesso_otp';
      
	/**
	 * Get tavle name
	 *
	 * @return string 
	 */
	public function getName() 
	{
		return $this->_name;
	}
 }