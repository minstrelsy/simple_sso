<?php
/**
 * OneTimePassword Model
 * 
 * @author sergio.rinaudo
 */
class Application_Model_Otp
{
	/**
	 * Table fields as class protected properties
	 */
	protected $_otp_id;
	protected $_data;
	protected $_context;
	protected $_otp_value;
	protected $_otp_data_insert;
	protected $_otp_data_consumed;
      
	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}
      
	/** 
	 * Magic method for setting property values
	 *
	 * @return void
	 */
	public function __set($name, $value)
	{
		$method = 'set' . str_replace("_","",$name);
          
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('__set: Invalid pages property '.$name.' method '.$method);
		}
          
		$this->$method($value);
	}
      
	/**
	 * Magic method for getting property values
	 *
	 * @return mixed
	 */
	public function __get($name)
	{
		$method = 'get' . str_replace("_","",$name);
          
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('__get: Invalid pages property '.$name.' method '.$method);
		}
          
		return $this->$method();
	}
      
	/**
	 * Set property values
	 *
	 * @return void
	 */
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);

		foreach($options as $key => $value) {
			$parts = explode("_",$key);
			$method = 'set';
			foreach($parts AS $part) {
				$method.= ucfirst($part);
			}
              
			if(in_array($method, $methods)) {
				$this->$method($value);
			}
		}
          
		return $this;
	}
      
	/**
	 * Set otp_id
	 *
	 * @return Application_Model_Otp
	 */
	public function setOtpId($otpId)
	{
		$this->_otp_id = (int)$otpId;
          
		return $this;
	}
      
	/**
	 * Get otp_id
	 *
	 * @return int
	 */
	public function getOtpId()
	{
		return $this->_otp_id;
	}
      
	/**
	 * Set the dara related with the otp
	 *
	 * @return Application_Model_Otp
	 */
	public function setData($data)
	{
		$this->_data = is_array($data) ? serialize($data) : serialize(array());
          
		return $this;
	}
      
	/**
	 * Get data
	 *
	 * @return int
	 */
	public function getData()
	{
		return unserialize($this->_data);
	}
      
	/**
	 * Set context
	 *
	 * @return Application_Model_Otp
	 */
	public function setContext($context)
	{
		$this->_context = (int)$context;
          
		return $this;
	}
      
	/**
	 * Get context
	 *
	 * @return int
	 */
	public function getContext()
	{
		return $this->_context;
	}
      
	/**
	 * Set otp_value
	 *
	 * @return Application_Model_Otp
	 */
	public function setOtpValue($otpValue)
	{
		$this->_otp_value = (string)$otpValue;
          
		return $this;
	}
      
	/**
	 * Get otp_value
	 *
	 * @return string
	 */
	public function getOtpValue()
	{
		return $this->_otp_value;
	}
      
	/**
	 * Set otp_data_insert
	 *
	 * @return Application_Model_Otp
	 */
	public function setOtpDataInsert($otpDataInsert)
	{
		$this->_otp_data_insert = $otpDataInsert;
          
		return $this;
	}
      
	/**
	 * Get otp_data_insert
	 *
	 * @return string
	 */
	public function getOtpDataInsert()
	{
		return $this->_otp_data_insert;
	}
      
	/**
	 * Set otp_data_consumed
	 *
	 * @return Application_Model_Otp
	 */
	public function setOtpDataConsumed($otpDataConsumed)
	{
		$this->_otp_data_consumed = $otpDataConsumed;
          
		return $this;
	}
      
	/**
	 * Get otp_data_consumed
	 *
	 * @return string
	 */
	public function getOtpDataConsumed()
	{
		return $this->_otp_data_consumed;
	}
}