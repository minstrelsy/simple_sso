<?php
/**
 * OtpMapper
 *
 * @author Sergio Rinaudo
 */
class Application_Model_OtpMapper
{
	/**
	 * DbTable
	 */
	protected $_dbTable;

	/**
	 * Context constants for Otp
	 */
	const TYPE_LOGIN = 1;

	/**
	 * Set dbTable object
	 *
	 * @return Application_Model_OtpMapper
	 */
	public function setDbTable($dbTable)
	{
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}

		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}

		$this->_dbTable = $dbTable;
		return $this;
	}
   
	/**
	 * Get dbTable object
	 *
	 * @return Zend_Db_Table_Abstract
	 */
	public function getDbTable()
	{
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_Otp');
		}

		return $this->_dbTable;
	}
   
	/**
	 * Insert or Update a row
	 *
	 * @return int Primary Key
	 */
	public function save( Application_Model_Otp $otp)
	{
		$data = array(
			'otp_id' => $otp->getOtpId(),
			'data' => $otp->getData(),
			'context' => $otp->getContext(),
			'otp_value' => $otp->getOtpValue(),
			'otp_data_insert' => $otp->getOtpDataInsert(),
			'otp_data_consumed' => $otp->getOtpDataConsumed()
		);

		$otpId = $otp->getOtpId();
		if (empty($otpId)) {
			unset( $data['otp_id'] );
			$otpId = $this->getDbTable()->insert($data);
			return $otpId;
		} else {
			unset( $data['otp_data_insert'] );
			$this->getDbTable()->update( $data, array('otp_id = ?' => $otpId));
			return $otpId;
		}
	}

	/**
	 * Generate an OTP
	 *
	 * @param array $data 
	 * @param int $context
	 * @return string $otp
	 */
	public function build($data, $context)
	{
      	$otp = md5(sha1(uniqid(rand(),true)));

      	$options = array(
			'data' => $data,
       		'context' => $context,
   			'otp_value' => $otp,
    		'otp_data_insert' => new Zend_Db_Expr('NOW()')
      	);

      	$otpModel = new Application_Model_Otp();
      	$otpModel->setOptions($options);
      	$this->save($otpModel);

      	return $otp;
	}
      
	/**
	 * Consume the OTP and return the row
	 *
	 * @param string $otp
	 * @param int $context 
	 * @return null|Zend_Db_Table_Row_Abstract
	 */
	public function consume($otp, $context)
	{
      	if(empty($otp)) return null;
      	
		$row = $this->fetchRowByOtp($otp,null); // row must not be consumed
          
		if(null === $row) return null;

		$data = array();
		$data['otp_data_consumed'] = new Zend_Db_Expr('NOW()');
		$where = $this->getDbTable()->getAdapter()->quoteInto('otp_value = ?', $otp);
		$this->getDbTable()->update($data, $where);
          
		return $row;
	}
      
	/**
	 * Return a row by an OTP
	 *
	 * @param bool $consumed null for a non-consumed row
	 * @return Zend_Db_Table_Row_Abstract
	 */
	public function fetchRowByOtp($otp, $consumed = null)
	{
		$select = $this->getDbTable()->select()->where('otp_value = ?', $otp);

		if(!empty($consumed)) {
			$select->where('otp_data_consumed NOT NULL');
		} else {
			$select->where('otp_data_consumed IS NULL');
		}

		$row = $this->getDbTable()->fetchRow($select);

		return $row;
	}
}